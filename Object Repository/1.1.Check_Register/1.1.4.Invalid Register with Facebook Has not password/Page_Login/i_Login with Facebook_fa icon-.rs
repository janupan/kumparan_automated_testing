<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Login with Facebook_fa icon-</name>
   <tag></tag>
   <elementGuidId>63f28fe8-3256-491f-aa40-8a5270248ff9</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa icon-fb</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[1]/div[1]/div[1]/div[@class=&quot;clearfix&quot;]/div[@class=&quot;container-blur padd-hor pos-rel&quot;]/div[@class=&quot;padd-top obj-center-pos&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;padd-hor padd-top&quot;]/div[@class=&quot;padd-hor margbot padd-bottom&quot;]/span[1]/button[@class=&quot;btn btn-block btn-lg btn-fb btn-glow metro&quot;]/i[@class=&quot;fa icon-fb&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='content']/div/div/div/div[2]/div[2]/div/div/div[2]/div/span/button/i</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login with Facebook'])[1]/i[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login with Google +'])[1]/preceding::i[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tersedia di :'])[1]/preceding::i[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//i</value>
   </webElementXpaths>
</WebElementEntity>
