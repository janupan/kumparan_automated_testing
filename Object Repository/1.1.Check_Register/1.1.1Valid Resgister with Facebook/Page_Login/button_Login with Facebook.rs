<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Login with Facebook</name>
   <tag></tag>
   <elementGuidId>cda84be1-cf80-43ac-9612-a248c59fb2ad</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-block btn-lg btn-fb btn-glow metro</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Login with Facebook</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[1]/div[1]/div[1]/div[@class=&quot;clearfix&quot;]/div[@class=&quot;container-blur padd-hor pos-rel&quot;]/div[@class=&quot;padd-top obj-center-pos&quot;]/div[@class=&quot;panel-body&quot;]/div[@class=&quot;padd-hor padd-top&quot;]/div[@class=&quot;padd-hor margbot padd-bottom&quot;]/span[1]/button[@class=&quot;btn btn-block btn-lg btn-fb btn-glow metro&quot;]</value>
   </webElementProperties>
</WebElementEntity>
