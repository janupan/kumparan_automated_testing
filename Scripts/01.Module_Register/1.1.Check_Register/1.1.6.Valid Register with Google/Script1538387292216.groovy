import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://kumparan.com/')

WebUI.click(findTestObject('1.1.Check_Register/1.1.6.Valid Register with Google/Page_Platform Media Berita Kolabora/a_Log In  Sign Up'))

WebUI.click(findTestObject('1.1.Check_Register/1.1.6.Valid Register with Google/Page_Login/span_Login with Google'))

WebUI.switchToWindowTitle('Masuk - Akun Google')

WebUI.setText(findTestObject('1.1.Check_Register/1.1.6.Valid Register with Google/Page_Masuk - Akun Google/input_kumparan.com_identifier'), 
    'panjaitanjanuary@gmail.com')

WebUI.click(findTestObject('1.1.Check_Register/1.1.6.Valid Register with Google/Page_Masuk - Akun Google/span_Berikutnya'))

WebUI.setEncryptedText(findTestObject('1.1.Check_Register/1.1.6.Valid Register with Google/Page_Masuk - Akun Google/input_Terlalu sering gagal_pas'), 
    'YGt3g+jjIGRMAtxUsOLcfA==')

WebUI.click(findTestObject('1.1.Check_Register/1.1.6.Valid Register with Google/Page_Masuk - Akun Google/svg_Terlalu sering gagal_VhKI7'))

WebUI.click(findTestObject('1.1.Check_Register/1.1.6.Valid Register with Google/Page_Masuk - Akun Google/span_Berikutnya'))

WebUI.click(findTestObject('1.1.Check_Register/1.1.6.Valid Register with Google/Page_Sign in - Google Accounts/content_Allow'))

